Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

To install in Chrome:

1. Sync this repository locally
2. Navigate to chrome://extensions/
3. Turn on developer mode (if it isn't already on) in the upper right
4. Click on "Load Unpacked" (upper left) and choose the folder for the repository

It's unfortunately not available in the web store directly due to Google's recondrite approval policies.
