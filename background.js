chrome.browserAction.onClicked.addListener(function(tab) {
	chrome.tabs.query( {"pinned": false}, function (tabs) {
		minIndex = tabs[0].index; // offset from pinned tabs
		tabs = tabs.sort((a, b) => (a.url > b.url) ? 1 : -1);
        	for (var i = 0; i < tabs.length; i++) {
			chrome.tabs.move(tabs[i].id, {"index": i + minIndex});
                }
	});
});
